﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneSwitcher : MonoBehaviour 
{
	public int sceneIndex = 0;
	public float waitTime = 5f;

	private void Start() 
	{
		StartCoroutine(GoToScene());
	}

	private IEnumerator GoToScene()
	{
		yield return new WaitForSeconds(waitTime);
		SceneManager.LoadScene(sceneIndex);
	}
}
