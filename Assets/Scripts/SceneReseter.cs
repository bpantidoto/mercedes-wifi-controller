﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

using EasyWiFi.Core;

public class SceneReseter : MonoBehaviour 
{
	public int sceneIndex = 1;

	private void ResetScene(StringBackchannelType stringBackchannel)
	{
		if(stringBackchannel.STRING_VALUE == "OK")
			return;
			
		if(stringBackchannel.STRING_VALUE == "Reset")
		{
			GameObject[] objectsToDestroy = GameObject.FindGameObjectsWithTag("Reset");

			for(int i = objectsToDestroy.Length-1; i>-1; i--)
			{
				//GameObject.Destroy(objectsToDestroy[i]);
			}

			SceneManager.LoadScene(sceneIndex);
		}
	}
}
