﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using EasyWiFi.ClientControls;

public class ForwardPlayerName : MonoBehaviour 
{
	public InputField _inputField;
	private StringDataClientController _controller;

	private void Awake() 
	{
		_controller = GetComponent<StringDataClientController>();
	}

	public void SetPlayerName()
	{
		_controller.setValue(_inputField.text);
	}
}
