﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using EasyWiFi.Core;

public class ColorChanger : MonoBehaviour 
{
	public Color none;

	public Color yellow;
	public Color red;
	public Color white;
	public Color green;
	public Color blue;
	public Color black;
	public Sprite[] foregroundSprites;

	private Image _image;
	private Image _foreground;
	
	private void Awake() 
	{
		_image = GetComponent<Image>();
		_foreground = transform.GetChild(0).GetComponent<Image>();
		_image.color =  none;
	}

	private void Update() 
	{
		if(Input.GetKeyDown(KeyCode.Alpha1))
		{
			ChangeSpriteColor(red);
		}
	}

	public void ChangeColor(StringBackchannelType data)
	{
		string colorName = data.STRING_VALUE;
		Color newColor = Color.black;
		int index = -1;

		switch(colorName)
		{
			case("yellow"):
				newColor = yellow;
				index = 0;
			break;

			case("red"):
				newColor = red;
				index = 1;
			break;

			case("white"):
				newColor = white;
				index = 2;
			break;

			case("green"):
				newColor = green;
				index = 3;
			break;

			case("blue"):
				newColor = blue;
				index = 4;
			break;

			case("black"):
				newColor = black;
			break;

			case("none"):
				newColor = none;
				index = -1;
			break;
		}

		if(index != -1)
		{
			ChangeSpriteColor(newColor);
			ChangeForegroundSprite(index);
		}else{
			ChangeSpriteColor(newColor);
			_foreground.gameObject.SetActive(false);
		}
	}
	
	private void ChangeSpriteColor(Color newColor)
	{
		_image.color = newColor;
	}

	private void ChangeForegroundSprite(int index)
	{
		_foreground.sprite = foregroundSprites[index];
		_foreground.gameObject.SetActive(true);
	}
}
