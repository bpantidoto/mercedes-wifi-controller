﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BackchanelTextChange : MonoBehaviour 
{
	private Text _textField;

	private void Awake() 
	{
		_textField = GetComponent<Text>();	
	}

	public void ChangeText(string text)
	{
		_textField.text = text;
	}
}
