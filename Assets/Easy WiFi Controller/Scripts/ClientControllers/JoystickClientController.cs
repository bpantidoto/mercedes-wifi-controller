﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using EasyWiFi.Core;
using CnControls;

namespace EasyWiFi.ClientControls
{
    [AddComponentMenu("EasyWiFiController/Client/UserControls/Joystick")]
    public class JoystickClientController : MonoBehaviour, IClientController
    {
        public string controlName = "Joystick1";
        JoystickControllerType joystick;       
        string joystickKey;
        private SimpleJoystick _joystick;

        private void Awake()
        {
            joystickKey = EasyWiFiController.registerControl(EasyWiFiConstants.CONTROLLERTYPE_JOYSTICK, controlName);
            joystick = (JoystickControllerType)EasyWiFiController.controllerDataDictionary[joystickKey];
            _joystick = GetComponent<SimpleJoystick>();
        }

        private void Update()
        {
            mapInputToDataStream();
        }

        public void mapInputToDataStream()
        {
            joystick.JOYSTICK_HORIZONTAL = _joystick.horizontal;
            joystick.JOYSTICK_VERTICAL = _joystick.vertical;
        }
    }
}