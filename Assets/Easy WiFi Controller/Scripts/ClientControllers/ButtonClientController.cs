﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using EasyWiFi.Core;

namespace EasyWiFi.ClientControls
{
    [AddComponentMenu("EasyWiFiController/Client/UserControls/Button")]
    public class ButtonClientController : MonoBehaviour, IClientController, IPointerDownHandler, IPointerUpHandler
    {
        public string controlName = "Button1";
        ButtonControllerType button;
        string buttonKey;
        bool pressed;

        private void Awake()
        {
            buttonKey = EasyWiFiController.registerControl(EasyWiFiConstants.CONTROLLERTYPE_BUTTON, controlName);
            button = (ButtonControllerType)EasyWiFiController.controllerDataDictionary[buttonKey];
        }

        public void OnPointerDown(PointerEventData pointerEventData)
        {
            pressed = true;
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            pressed = false;
        }

        private void Update()
        {
            mapInputToDataStream();
        }

        public void mapInputToDataStream()
        {
            if (pressed)
            {
                button.BUTTON_STATE_IS_PRESSED = true;
            }
            else
            {
                button.BUTTON_STATE_IS_PRESSED = false;
            }            
        }
    }
}